package net.oschina.md2.util;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.List;

import net.oschina.md2.export.HTMLDecorator;
import net.oschina.md2.markdown.Block;
import net.oschina.md2.markdown.MDAnalyzer;

public class MDUtil {

	public static String markdown2Html(String mdStr){
		if(mdStr==null){
			return null;
		}
		BufferedReader reader = new BufferedReader(new StringReader(mdStr));
		List<Block> list = MDAnalyzer.analyze(reader);
		
		HTMLDecorator decorator = new HTMLDecorator(); 
		
		decorator.decorate(list);
		
		return decorator.outputHtml();
	}
	
	public static void main(String[] args) {
		System.out.println(MDUtil.markdown2Html("[**开源中国**](http://www.oschina.net)社区，是一个很不错的网站。欢迎上去查找开源软件，吐吐槽！"));
	}
}

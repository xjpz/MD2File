package net.oschina.md2.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;

import net.oschina.md2.markdown.Block;
import net.oschina.md2.markdown.MDAnalyzer;

/**
 * 文档生成工厂
 * @author yangyingqiang
 * @time 2015年5月1日 下午2:35:08
 *
 */
public class FileFactory {
	
	public static void produce(File file, String outputFilePath) throws FileNotFoundException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		produce(reader, outputFilePath);
	}
	
	public static void produce(InputStream is, String outputFilePath) throws FileNotFoundException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		
		produce(reader, outputFilePath);
	}
	
	public static void produce(String mdText, String outputFilePath) throws FileNotFoundException{
		BufferedReader reader = new BufferedReader(new StringReader(mdText));
		
		produce(reader, outputFilePath);
	}
	
	public static void produce(BufferedReader reader, String outputFilePath) throws FileNotFoundException{
		List<Block> list = MDAnalyzer.analyze(reader);
		produce(list, outputFilePath);
	}
	
	public static void produce(List<Block> list, String outputFilePath) throws FileNotFoundException{
		String ext = getExtOfFile(outputFilePath);
		Decorator decorator = BuilderFactory.build(ext);

		decorator.beginWork(outputFilePath);
		decorator.decorate(list);
		decorator.afterWork(outputFilePath);
	}
	
	private static String getExtOfFile(String outputFilePath){
		if(outputFilePath==null){
			return "";
		}
		int i = outputFilePath.lastIndexOf(".");
		if(i < 0){
			return "";
		}
		return outputFilePath.substring(i+1);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
//		WordFactory.produce("#大标题\n###小标题\n```\npublic void main()\n    system.out.println(\"hello\")\n```\n123123\n```\n正文，这里是**_正文_**测试测试**粗体**哦\n> 这里是引用，**粗体**_斜体__**粗斜合一**_**123_12", "/Users/cevin/Downloads/simple4.docx");
		FileFactory.produce(new File("/Users/cevin/Downloads/测试.md"), "/Users/cevin/Downloads/simple.docx");
//		WordFactory.produce("> ###123123\n> 123","/Users/cevin/Downloads/simple2.docx");
	}
}

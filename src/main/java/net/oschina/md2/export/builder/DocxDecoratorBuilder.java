package net.oschina.md2.export.builder;

import net.oschina.md2.export.Decorator;
import net.oschina.md2.export.DocxDecorator;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class DocxDecoratorBuilder implements DecoratorBuilder{

	public Decorator build() {
		return new DocxDecorator(new XWPFDocument());
	}

}
